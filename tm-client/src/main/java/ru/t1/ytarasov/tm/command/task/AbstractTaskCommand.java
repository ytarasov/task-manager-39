package ru.t1.ytarasov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return getServiceLocator().getTaskEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public @Nullable Role @Nullable [] getRoles() {
        return Role.values();
    }

    public void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId() + ";");
        System.out.println("Name: " + task.getName() + ";");
        System.out.println("Description: " + task.getDescription() + ";");
        System.out.println("Status: " + Status.toName(task.getStatus()));
        System.out.println("Created: " + task.getCreated());
        System.out.println("Project ID: " + task.getProjectId());
    }

}
