package ru.t1.ytarasov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.ytarasov.tm.api.service.ICommandService;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @NotNull
    public ISystemEndpoint getSystemEndpoint() {
        return getServiceLocator().getSystemEndpoint();
    }

    @Nullable
    @Override
    public Role @Nullable [] getRoles() {
        return null;
    }

}
