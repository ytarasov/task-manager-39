package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ytarasov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.dto.request.project.*;
import ru.t1.ytarasov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ytarasov.tm.dto.response.project.*;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.marker.SoapCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.service.PropertyService;

import java.util.List;
import java.util.UUID;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private static final String TEST_USER_LOGIN = "TEST";

    @NotNull
    private static final String TEST_USER_PASSWORD = "TEST";

    @NotNull
    private static final String NEW_PROJECT_NAME = "New project";

    @NotNull
    private static final String NEW_PROJECT_DESCRIPTION = "New project";

    @NotNull
    private static final String FAKE_PROJECT_ID = "FAKE";

    @NotNull
    private static final String UPDATE_PROJECT_NAME = "Update project";

    @NotNull
    private static final String UPDATE_PROJECT_DESCRIPTION = "Update project";

    @Nullable
    private String testToken;

    @NotNull
    private final String badToken = UUID.randomUUID().toString();

    @Before
    public void setup() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(TEST_USER_LOGIN, TEST_USER_PASSWORD);
        testToken = authEndpoint.login(userLoginRequest).getToken();
    }

    @After
    public void tearDown() {
        @NotNull final UserLogoutRequest userLogoutRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(userLogoutRequest);
    }

    @Test
    public void createProject() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(testToken);
        request.setName(NEW_PROJECT_NAME);
        request.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final ProjectCreateRequest badRequest = new ProjectCreateRequest();
        badRequest.setName(NEW_PROJECT_NAME);
        badRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final ProjectCreateRequest badRequest1 = new ProjectCreateRequest(badToken);
        badRequest1.setName(NEW_PROJECT_NAME);
        badRequest1.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final ProjectCreateRequest badRequest2 = new ProjectCreateRequest(badToken);
        badRequest2.setName(null);
        badRequest2.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final ProjectCreateRequest badRequest3 = new ProjectCreateRequest(badToken);
        badRequest3.setName("");
        badRequest3.setDescription(NEW_PROJECT_DESCRIPTION);
        @NotNull final ProjectCreateRequest badRequest4 = new ProjectCreateRequest(badToken);
        badRequest4.setName(NEW_PROJECT_NAME);
        badRequest4.setDescription(null);
        @NotNull final ProjectCreateRequest badRequest5 = new ProjectCreateRequest(badToken);
        badRequest5.setName(NEW_PROJECT_NAME);
        badRequest5.setDescription("");
        @Nullable final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Assert.assertNotNull(response);
        @Nullable final Project project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(badRequest));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(badRequest1));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(badRequest2));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(badRequest3));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(badRequest4));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(badRequest5));
    }

    @Test
    public void listProject() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(testToken);
        @NotNull final ProjectListRequest badRequest = new ProjectListRequest(badToken);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(badRequest));
        @Nullable final ProjectListResponse response = projectEndpoint.listProject(request);
        Assert.assertNotNull(response);
        @Nullable final List<Project> projects = response.getProjects();
        Assert.assertNotNull(projects);
        @NotNull final UserLogoutRequest userLogoutRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(userLogoutRequest);
    }

    @Test
    public void showProjectById() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(testToken);
        projectCreateRequest.setName(NEW_PROJECT_NAME);
        projectCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Project project = projectEndpoint.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(testToken);
        request.setProjectId(project.getId());
        @NotNull final ProjectShowByIdRequest badRequest = new ProjectShowByIdRequest();
        @NotNull final ProjectShowByIdRequest badRequest1 = new ProjectShowByIdRequest(badToken);
        badRequest1.setProjectId(project.getId());
        @NotNull final ProjectShowByIdRequest badRequest2 = new ProjectShowByIdRequest(testToken);
        badRequest2.setProjectId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.showProjectById(badRequest));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.showProjectById(badRequest1));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.showProjectById(badRequest2));
        @Nullable final ProjectShowByIdResponse response = projectEndpoint.showProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project checkProject = response.getProject();
        Assert.assertNotNull(checkProject);
        Assert.assertEquals(project.getId(), checkProject.getId());
    }

    @Test
    public void startProjectById() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(testToken);
        projectCreateRequest.setName(NEW_PROJECT_NAME);
        projectCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Project project = projectEndpoint.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(testToken);
        request.setProjectId(project.getId());
        @NotNull final ProjectStartByIdRequest badRequest = new ProjectStartByIdRequest();
        @NotNull final ProjectStartByIdRequest badRequest1 = new ProjectStartByIdRequest(badToken);
        badRequest1.setProjectId(project.getId());
        @NotNull final ProjectStartByIdRequest badRequest2 = new ProjectStartByIdRequest(testToken);
        badRequest2.setProjectId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(badRequest));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(badRequest1));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(badRequest2));
        @Nullable final ProjectStartByIdResponse response = projectEndpoint.startProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project startedProject = response.getProject();
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.IN_PROGRESS.toString(), startedProject.getStatus().toString());
    }

    @Test
    public void completeProjectById() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(testToken);
        projectCreateRequest.setName(NEW_PROJECT_NAME);
        projectCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Project project = projectEndpoint.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(testToken);
        request.setProjectId(project.getId());
        @NotNull final ProjectCompleteByIdRequest badRequest = new ProjectCompleteByIdRequest();
        @NotNull final ProjectCompleteByIdRequest badRequest1 = new ProjectCompleteByIdRequest(badToken);
        @NotNull final ProjectCompleteByIdRequest badRequest2 = new ProjectCompleteByIdRequest(testToken);
        badRequest2.setProjectId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectById(badRequest));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectById(badRequest1));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectById(badRequest2));
        @Nullable final ProjectCompleteByIdResponse response = projectEndpoint.completeProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project startedProject = response.getProject();
        Assert.assertNotNull(startedProject);
        Assert.assertEquals(Status.COMPLETED.toString(), startedProject.getStatus().toString());
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(testToken);
        projectCreateRequest.setName(NEW_PROJECT_NAME);
        projectCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Project project = projectEndpoint.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        @NotNull final ProjectCompleteByIdRequest projectCompleteByIdRequest = new ProjectCompleteByIdRequest(testToken);
        projectCompleteByIdRequest.setProjectId(project.getId());
        @Nullable final Project completedProject = projectEndpoint.completeProjectById(projectCompleteByIdRequest).getProject();
        Assert.assertNotNull(completedProject);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(testToken);
        request.setProjectId(project.getId());
        request.setStatus(Status.IN_PROGRESS);
        @NotNull final ProjectChangeStatusByIdRequest badRequest = new ProjectChangeStatusByIdRequest();
        @NotNull final ProjectChangeStatusByIdRequest badRequest1 = new ProjectChangeStatusByIdRequest(badToken);
        @NotNull final ProjectChangeStatusByIdRequest badRequest2 = new ProjectChangeStatusByIdRequest(testToken);
        badRequest2.setProjectId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(badRequest));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(badRequest1));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(badRequest2));
        @Nullable final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(request);
        Assert.assertNotNull(response);
        @Nullable final Project changedStatusProject = response.getProject();
        Assert.assertNotNull(changedStatusProject);
        Assert.assertNotEquals(completedProject.getStatus().toString(), changedStatusProject.getStatus().toString());
    }

    @Test
    public void removeById() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(testToken);
        projectCreateRequest.setName(NEW_PROJECT_NAME);
        projectCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Project project = projectEndpoint.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(testToken);
        request.setProjectId(project.getId());
        @NotNull final ProjectRemoveByIdRequest badRequest = new ProjectRemoveByIdRequest();
        @NotNull final ProjectRemoveByIdRequest badRequest1 = new ProjectRemoveByIdRequest(badToken);
        @NotNull final ProjectRemoveByIdRequest badRequest2 = new ProjectRemoveByIdRequest(testToken);
        badRequest2.setProjectId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(badRequest));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(badRequest1));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(badRequest2));
        @Nullable final ProjectRemoveByIdResponse response = projectEndpoint.removeProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project removedProject = response.getProject();
        Assert.assertNotNull(removedProject);
    }

    @Test
    public void updateById() {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(testToken);
        projectCreateRequest.setName(NEW_PROJECT_NAME);
        projectCreateRequest.setDescription(NEW_PROJECT_DESCRIPTION);
        @Nullable final Project project = projectEndpoint.createProject(projectCreateRequest).getProject();
        Assert.assertNotNull(project);
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(testToken);
        request.setProjectId(project.getId());
        request.setName(UPDATE_PROJECT_NAME);
        request.setDescription(UPDATE_PROJECT_DESCRIPTION);
        @NotNull final ProjectUpdateByIdRequest badRequest = new ProjectUpdateByIdRequest();
        @NotNull final ProjectUpdateByIdRequest badRequest1 = new ProjectUpdateByIdRequest(badToken);
        @NotNull final ProjectUpdateByIdRequest badRequest2 = new ProjectUpdateByIdRequest(testToken);
        badRequest2.setProjectId(FAKE_PROJECT_ID);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(badRequest));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(badRequest1));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(badRequest2));
        @Nullable final ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
    }

}
