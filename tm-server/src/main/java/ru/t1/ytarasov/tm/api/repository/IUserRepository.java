package ru.t1.ytarasov.tm.api.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @NotNull
    @Select("SELECT * FROM app_user")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "role", column = "role"),
            @Result(property = "isLocked", column = "locked")
    })
    List<User> findAll();

    @Select("SELECT COUNT(*) FROM app_user")
    int getSize();

    @Insert("INSERT INTO app_user " +
            "(id, login, password_hash, first_name, last_name, middle_name, role, locked) " +
            "VALUES(#{id}, #{login}, #{passwordHash}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void add(@NotNull User model);

    @Nullable
    @Select("SELECT * FROM app_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "role", column = "role"),
            @Result(property = "isLocked", column = "locked")
    })
    User findOneById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM app_user")
    void clear();

    @Nullable
    @Select("SELECT * FROM app_user WHERE login = #{login}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "role", column = "role"),
            @Result(property = "isLocked", column = "locked")
    })
    User findByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM app_user WHERE email = #{email}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "role", column = "role"),
            @Result(property = "isLocked", column = "locked")
    })
    User findByEmail(@NotNull @Param("email") String email);

    @Delete("DELETE FROM app_user WHERE id = #{id}")
    void remove(@NotNull final User user);

    @Delete("DELETE FROM app_user WHERE login = #{login}")
    void removeByLogin(@Nullable @Param("login") String login);

    @NotNull
    @Update("UPDATE app_user SET login = #{login}, password_hash = #{passwordHash}, first_name = #{firstName}, " +
            "last_name = #{lastName}, middle_name = #{middleName}, role = #{role}, locked = #{isLocked}" +
            " WHERE id = #{id}")
    @SneakyThrows
    void update(@NotNull User user);

}
