package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws Exception;

    @NotNull
    Project removeProjectById(@Nullable String userId, @Nullable String projectId) throws Exception;

    void clearAllProjects(@Nullable String userId) throws Exception;

    void unbindTaskFromProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws Exception;

}
