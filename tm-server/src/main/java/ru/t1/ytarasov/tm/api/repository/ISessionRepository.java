package ru.t1.ytarasov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.Session;

public interface ISessionRepository {

    @Insert("INSERT INTO app_session (id, role, timestamp, user_id) VALUES(#{id}, #{role}, #{date}, #{userId})")
    void add(@NotNull final Session session);

    @Nullable
    @Select("SELECT * FROM app_session WHERE id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "role", column = "role"),
            @Result(property = "date", column = "timestamp"),
            @Result(property = "userId", column = "user_id")
    })
    Session findOneById(@NotNull @Param("id") final String id);

    @Delete("DELETE FROM app_session WHERE id = #{id}")
    void remove(@NotNull final Session session);

}
