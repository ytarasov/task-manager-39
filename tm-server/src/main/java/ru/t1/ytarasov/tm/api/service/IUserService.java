package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.model.User;

import java.util.List;

public interface IUserService {

    @NotNull
    User add(@Nullable final User user) throws Exception;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable final String email,
            @Nullable Role role
    ) throws Exception;

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable final String id) throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws Exception;

    @Nullable
    User removeByLogin(@Nullable String login) throws Exception;

    int getSize() throws Exception;

    @NotNull
    User remove(@Nullable final User user) throws Exception;

    @NotNull
    User removeById(@Nullable final String id) throws Exception;

    @Nullable
    User removeByEmail(@Nullable String email) throws Exception;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws Exception;

    boolean isLoginExist(@Nullable String login) throws Exception;

    boolean isEmailExists(@Nullable String email) throws Exception;

    void lockUser(@Nullable String login) throws Exception;

    void unlockUser(@Nullable String login) throws Exception;

    void clear();

}
