package ru.t1.ytarasov.tm.api.service;

import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws Exception;

    @Nullable
    List<Project> findAll();

    @Nullable
    List<Project> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable final Comparator comparator) throws Exception;

    @Nullable
    List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable final Sort sort) throws Exception;

    int getSize() throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws Exception;

    @Nullable
    Project findOneById(@Nullable final String id) throws Exception;

    @Nullable
    Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

    @Nullable
    Project remove(@Nullable final Project project) throws Exception;

    @Nullable
    Project removeById(@Param("id") @Nullable final String id) throws Exception;

    @Nullable
    Project removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

    boolean existsById(@Nullable final String id) throws Exception;

    boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception;

    @NotNull
    Project add(@Nullable Project project) throws Exception;

    Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception;

    void clear() throws Exception;

    void clear(@Nullable final String userId) throws Exception;

}