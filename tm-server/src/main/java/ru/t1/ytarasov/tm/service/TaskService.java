package ru.t1.ytarasov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.ITaskService;
import ru.t1.ytarasov.tm.comparator.CreatedComparator;
import ru.t1.ytarasov.tm.comparator.NameComparator;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public @NotNull Task add(@Nullable Task task) throws Exception {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId,
                       @Nullable final String name,
                       @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        @NotNull List<Task> tasks = new ArrayList<>();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            tasks = taskRepository.findAll();
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Task> tasks = new ArrayList<>();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            tasks = taskRepository.findAllWithUserId(userId);
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        List<Task> tasks = new ArrayList<>();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            if (comparator == NameComparator.INSTANCE) tasks = repository.findAllOrderByName();
            else if (comparator == CreatedComparator.INSTANCE) tasks = repository.findAllOrderByCreated();
            else tasks = repository.findAllOrderByStatus();
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        List<Task> tasks = new ArrayList<>();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            if (comparator == NameComparator.INSTANCE) tasks = repository.findAllWithUserIdOrderByName(userId);
            else if (comparator == CreatedComparator.INSTANCE) tasks = repository.findAllWithUserIdOrderByCreated(userId);
            else tasks = repository.findAllWithUserIdOrderByStatus(userId);
        } finally {
            sqlSession.close();
        }
        return tasks;
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable Sort sort) throws Exception {
        if (sort == null) return findAll();
        else return findAll(sort.getComparator());
    }

    @Override
    public @Nullable List<Task> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        else return findAll(userId, sort.getComparator());
    }

    @Override
    public @Nullable Task findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Task task;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneById(id);
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Override
    public @Nullable Task findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        Task task;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            task = repository.findOneByIdWithUserId(userId, id);
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Override
    public int getSize() throws Exception {
        int size;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            size = repository.getSize();
        } finally {
            sqlSession.close();
        }
        return size;
    }

    @Override
    public int getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        int size;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            size = repository.getSizeWithUserId(userId);
        } finally {
            sqlSession.close();
        }
        return size;
    }

    @Override
    public @NotNull Task remove(@Nullable Task task) throws Exception {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.remove(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @Nullable
    @Override
    public Task removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public @Nullable Task removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Override
    public @NotNull List<Task> findAllTasksByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllTasksByProjectId(userId, projectId);
        }
    }

    @Override
    @NotNull
    public Task update(@NotNull final Task task) throws Exception {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return task;
    }

    @NotNull
    @Override
    public Task updateById(@Nullable final String userId,
                           @Nullable final String id,
                           @Nullable final String name,
                           @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return update(task);
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(@Nullable final String userId,
                                     @Nullable final String id,
                                     @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        @Nullable final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return update(task);
    }

    @Override
    public void clear() throws Exception {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clearWithUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
