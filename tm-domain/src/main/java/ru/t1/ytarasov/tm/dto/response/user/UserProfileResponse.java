package ru.t1.ytarasov.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.response.AbstractResponse;
import ru.t1.ytarasov.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserProfileResponse extends AbstractResponse {

    @Nullable
    private User user;

    public UserProfileResponse(@Nullable User user) {
        this.user = user;
    }

}
